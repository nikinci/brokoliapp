package com.nikinci.brokoliapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nikinci.brokoliapp.R;
import com.nikinci.brokoliapp.activity.SecondActivity;
import com.nikinci.brokoliapp.adapter.HomeRecyclerAdapter;
import com.nikinci.brokoliapp.utils.BrokoliUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nikinci on 02/01/2017.
 */

public class HomePagerItemFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    HomeRecyclerAdapter recyclerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private final int MAX_ITEM_SIZE = 25;
    List<Integer> colorList;

    public static HomePagerItemFragment newInstance() {

        Bundle args = new Bundle();

        HomePagerItemFragment fragment = new HomePagerItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflater.inflate(R.layout.fragment_home_item, container, false);
        final View rootView = inflater.inflate(R.layout.fragment_home_item, container, false);
        ButterKnife.bind(this, rootView);
        populateUi(rootView);
        return rootView;
    }

    private void populateUi(View rootView) {
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        colorList = new ArrayList<>();
        recyclerAdapter = new HomeRecyclerAdapter(colorList);
        recyclerView.setAdapter(recyclerAdapter);
        generateItems();
        recyclerAdapter.setOnItemClickListener(new HomeRecyclerAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Integer color, int positon) {
                startActivity(SecondActivity.newIntent(getContext(), color));
            }
        });
    }


    private void generateItems() {
        colorList.clear();
        int randomInt = BrokoliUtil.getRandomInt(MAX_ITEM_SIZE);
        for (int i = 0; i < randomInt; i++) {
            colorList.add(BrokoliUtil.getRandomColor());
        }
        recyclerAdapter.notifyDataSetChanged();
    }
}
