package com.nikinci.brokoliapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nikinci.brokoliapp.R;
import com.nikinci.brokoliapp.view.SquareRelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nikinci on 02/01/2017.
 */

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.HomeViewHolder> {


    private List<Integer> colorList = new ArrayList<>();
    private ItemClickListener onItemClickListener;

    public interface ItemClickListener {
        void onItemClick(Integer color, int positon);
    }


    public HomeRecyclerAdapter(List<Integer> colorList) {
        this.colorList = colorList;
    }

    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_home_recycler, parent, false);
        HomeViewHolder viewHolder = new HomeViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HomeViewHolder holder, int position) {

        Integer color = colorList.get(position);
        holder.squareRelativeLayout.setBackgroundColor(color);
        if (onItemClickListener != null) {

            holder.squareRelativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int adapterPosition = holder.getAdapterPosition();
                    onItemClickListener.onItemClick(colorList.get(adapterPosition), adapterPosition);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.squareRelativeLayout)
        SquareRelativeLayout squareRelativeLayout;

        public HomeViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setOnItemClickListener(ItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
