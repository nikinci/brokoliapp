package com.nikinci.brokoliapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nikinci.brokoliapp.fragment.HomePagerItemFragment;

/**
 * Created by nikinci on 02/01/2017.
 */

public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private int size;


    public HomePagerAdapter(FragmentManager fm, int size) {
        super(fm);
        this.size = size;
    }

    @Override
    public Fragment getItem(int position) {
        return HomePagerItemFragment.newInstance();
    }

    @Override
    public int getCount() {
        return size;
    }
}
