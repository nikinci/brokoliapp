package com.nikinci.brokoliapp.utils;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by nikinci on 02/01/2017.
 */

public class BrokoliUtil {


    private BrokoliUtil() {
    }

    public  static int getRandomInt(int maxValue) {
        Random generator = new Random();
        int i = generator.nextInt(maxValue) + 1;
        return i;
    }

    public static int getRandomColor() {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        return color;
    }

}
