package com.nikinci.brokoliapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.nikinci.brokoliapp.R;

import butterknife.BindView;

/**
 * Created by nikinci on 02/01/2017.
 */

public class SecondActivity extends BaseActivity {

    @BindView(R.id.linearLayoutRoot)
    LinearLayout linearLayoutRoot;
    private Integer color;
    private final static String BUNDLE_KEY_COLOR = "key.color";

    public static Intent newIntent(Context context, Integer color) {

        Intent intent = new Intent(context, SecondActivity.class);
        intent.putExtra(BUNDLE_KEY_COLOR, color);
        return intent;
    }

    @Override
    protected void populateUi() {
        this.color = getIntent().getIntExtra(BUNDLE_KEY_COLOR, 0);
        linearLayoutRoot.setBackgroundColor(color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_second;
    }
}
