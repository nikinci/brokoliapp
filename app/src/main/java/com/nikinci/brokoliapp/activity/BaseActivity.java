package com.nikinci.brokoliapp.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by nikinci on 02/01/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private int layoutResId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        populateUi();

    }

    protected abstract void populateUi();

    @LayoutRes
    public abstract int getLayoutResId();
}
