package com.nikinci.brokoliapp.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nikinci.brokoliapp.R;
import com.nikinci.brokoliapp.adapter.HomePagerAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;

public class MainActivity extends BaseActivity {


    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.pageIndicator)
    CirclePageIndicator pageIndicator;
    HomePagerAdapter pagerAdapter;

    private final int PAGE_SIZE = 3;


    @Override
    protected void populateUi() {
        pagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), PAGE_SIZE);
        viewPager.setAdapter(pagerAdapter);
        pageIndicator.setViewPager(viewPager);

    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }


}
